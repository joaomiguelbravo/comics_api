# frozen_string_literal: true

class ComicsSerializer < ActiveModel::Serializer
  # @param [Object] collection
  def initialize(collection)
    @collection = collection
  end

  def serialize
    @collection.map do |record|
      record.merge(favorite: Comic.find_by(api_comic_id: record['id'])&.favorite || false)
    end
  end
end
