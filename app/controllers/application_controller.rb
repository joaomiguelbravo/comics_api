# frozen_string_literal: true

class ApplicationController < ActionController::API # :nodoc:
 def status
  render json: { data: 'ok' }
 end
end
