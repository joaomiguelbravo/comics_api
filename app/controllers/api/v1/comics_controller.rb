# frozen_string_literal: true

module Api
  module V1
    class ComicsController < ApplicationController
      def index
        filtered_params = permitted_params
        api_service = ExternalApiCommunicator.new('index', filtered_params)

        if api_service.call
          api_service.data['results'] = ComicsSerializer.new(api_service.data['results']).serialize
          render status: :ok, json: { data: api_service.data }
        else
          render status: :bad_request, message: api_service.error
        end
      end

      def update
        filtered_params = permitted_params

        comic = Comic.find_or_create_by(api_comic_id: filtered_params[:comic_id])

        comic.update(favorite: filtered_params[:favorite])

        render status: :ok, message: 'Record updated with success'
      end

      private

      def permitted_params
        params.permit(:favorite, :title, :comic_id, :limit, :offset)
      end
    end
  end
end
