# frozen_string_literal: true

# Service that aggregates all prefix from requests
require 'httparty'

class AggregatorService
  include HTTParty
  BUSINESS_API = 'https://challenge-business-sector-api.meza.talkdeskstg.com/sector/'

  attr_reader :error, :data

  def initialize(phones)
    @phones = phones
    @data = {}
    @error = {}
    @file = prefix_file_handler
  end

  def call
    @phones.each do |phone_number|
      phone = Phone.new(number: phone_number)
      next unless phone.valid?

      request = request_api(phone.number)

      data_handler(JSON.parse(request.body), find_prefix(phone.number)) if request.code == 200
    end

    true
  rescue StandardError => e
    @error = e

    false
  end

  private

  def request_api(phone_number)
    HTTParty.get((BUSINESS_API + phone_number).to_s)
  end

  def data_handler(response, prefix)
    if @data[prefix].present?
      @data[prefix][response['sector']] = if @data[prefix][response['sector']].present?
                                            @data[prefix][response['sector']] + 1
                                          else
                                            1
                                          end
    else
      @data[prefix] = { response['sector'] => 1 }
    end
  end

  def find_prefix(phone_number, prefix_position = 0)
    # Remove characters to search on file and add one more number each time the function its called
    prefix = phone_number.gsub('+', '').gsub('0', '')[0..prefix_position]

    raise StandardError, 'This prefix is not valid' if phone_number.size == prefix_position

    # Find prefix on file, or call this function again to add another number to the prefix for a new search
    @file.lazy.find { |line| line.chomp == prefix }&.chomp || find_prefix(phone_number, prefix_position + 1)
  end

  def prefix_file_handler
    file_to_read = if Rails.env == 'test'
                     'storage/prefixes_smaller.txt'
                   else
                     'storage/prefixes.txt'
                   end

    IO.foreach(file_to_read)
  end
end
