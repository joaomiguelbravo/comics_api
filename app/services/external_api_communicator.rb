# frozen_string_literal: true

require 'httparty'

class ExternalApiCommunicator
  include HTTParty
  API_URL = "https://gateway.marvel.com/v1/public/comics?ts=1000&apikey=#{ENV['MARVEL_PUBLIC_KEY']}&hash="
  MD5_STRING = '1000cb116416e7b6451df4226e110970d61e15dcf22f07e3e205bebd46de31d15ee9a76d85c2'

  attr_reader :data, :error

  def initialize(request, params = {})
    @url = build_url(params)
    @request = request
  end

  def call
    response = send(@request)

    if response['code'] == 200
      @data = response['data']
      true
    else
      @error = response['message']
      false
    end
  end

  def index
    HTTParty.get(@url)
  end

  private

  def build_url(params = {})
    md5 = Digest::MD5.new
    url_param_string = ''
    params.each do |key, value|
      next if value.empty?

      url_param_string += "&#{key}=#{value}"
    end

    "#{API_URL}#{md5.hexdigest(MD5_STRING)}" + url_param_string
  end
end
