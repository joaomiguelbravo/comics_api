class CreateComics < ActiveRecord::Migration[6.0]
  def change
    create_table :comics do |t|
      t.integer :api_comic_id
      t.string :name
      t.boolean :favorite
      t.timestamps
    end
  end
end
